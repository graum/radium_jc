# Laser Cooling of Radium Ions

M. Fan, C. A. Holliman, A. L. Wang, and A. M. Jayich

![i](./img/reference.png)

---

## Who did this?

- Jayich group at UCSB ![i](./img/jayich.jpeg) <!-- .element height="10%" width="10%" -->

- part of the "radiactive gang" of ion trappers in southern California: Eric Hudson (UCLA, $^{229}$Th), Wes Cambell (UCLA, $^{133}$Ba)

 ![i](./img/campbell.png) <!-- .element height="10%" width="10%" -->![i](./img/hudson.png) <!-- .element height="10%" width="10%" -->

---

## Why Radium?

- Visible: detection at<span style="color:rgb(0,160, 255)"> 468 nm </span>
- Lots of protons
- $^{225}$Ra has an octupole deformed nucleus
- $^{225}$Ra has nuclear spin $I=1/2$
  - Large `$^{2}\text{P}_{1/2}$` hyperfine splitting of 5.4 GHz
  - suppress readout errors by a factor of 8 compared to Yb$^+$ (2.1 GHz)

===

## Why Not Radium?

- It's *very* radiactive and will kill you
  
![Death](img/skull.jpg)<!-- .element height="20%" width="20%" -->
  - Half life of most stable isotope ($^{226}$Ra) is 1600 years
  - Half life of interesting isotope ($^{225}$Ra) is 14.9 **days**

===

### Aside: Why not $^{133}$Ba?

- $^{133}$Ba has a half life of 10.5 years <!-- .element: style="font-size:70%;" -->
  - still, that's really radioactive
- Also has nuclear spin of $I=1/2$ <!-- .element: style="font-size:70%;" -->
- Detection wavelength of 493 nm <!-- .element: style="font-size:70%;" -->
  - $^{2}\text{P}_{1/2}$ hyperfine splitting of 1.8 GHz <!-- .element: style="font-size:70%;" -->
  - But with shelving to $^{2}\text{D}_{5/2}$ via optical pumping, can demonstrate record SPAM fidelity of $\mathcal{F} = 0.99971(6)$ <!-- .element: style="font-size:70%;" -->


![arXiv:1907.13331](img/barium_SPAM.png)

---

## What does the paper cover?

- load radium using laser ablation
- measure the $^{226}$Ra `$7\text{s}^{2}\text{S}_{1/2} \to 7\text{p}^{2}\text{P}_{1/2}$` transition frequency
- measure the `$^{2}\text{P}_{1/2}$` branching ratio to `$^{2}\text{S}_{1/2}$` and `$^{2}\text{D}_{3/2}$`

---

### Loading Radium

- Previously the ISOLDE beamline at CERN measured radium isotopes 209-214
- KVI Gronigen used TRI$\mu$P beamline to study radium 212-214
  - This seems really hard!
- Here the UCSB group applies the much simpler technique of **laser ablation** to trap $^{226}$Ra, the most stable isotope

===

#### Sample Preparation

- use $^{226}\text{RaCl}_2$ dissolved in 5mL of 0.1M $\text{HCl}$
- activity of $10 \mu\text{Cu}$ (about 10 $\mu$g of radium)
  - 370,000 decay events per second
  - human body has an activity of about $0.2 \mu\text{Cu}$ (from $^{40}\text{K}$ and $^{14}\text{C}$)
  - Modern $^{241}\text{Am}$ smoke detector mught have $1 \mu\text{Cu}$
  - probably dangerous to work with, not dangerous to be work around
- dry solution onto a stainless steel target 

===

#### Ablation

- Ablation the $^{226}\text{Ra}$/SS target
  - 10mJ, 532 nm, pulsed YAG
  - 0.5 mm diameter
- switch on trap RF 20 $\mu$s after ablation pulse

![Trap](img/trap.png) <!-- .element height="30%" width="30%" -->

---

### Laser Cooling Radium

- Cool and detect with <span style="color:rgb(0,160, 255)"> 468 nm </span>, repump with <span style="color:rgb(100,10, 25)"> 1079 nm </span>
- task is now to get a good measurement 468 nm transition wavelength and the $^{2}\text{P}_{1/2}$ branching ratio <!-- .element style="float: left"-->

![Laser Cooling](img/laser_cooling.png)<!-- .element height="40%" width="40%" style="float: right"-->
![Radium Ion Chain](img/radium_chain.png) <!-- .element height="40%" width="40%" style="float: left" -->

---

### Branching Ratio
![Branching Ratio](img/branching_ratio.png)<!-- .element height="50%" width="50%" style="float: right"-->
<div style="width: 50%; font-size: 60%">
<ul>
<li>pump population to the D state, and then back to the S state, while colleting time tagged photons</li>
<li>$N_b = N_b^F - N_b^B$ is probe</li>
<li>$N_r = N_r^F - N_r^B$ is "reset"</li>
<li>Branching ratio to ground state is $p=N_b / (N_b + N_r) = 0.9104(5)$</li>
<li>Detection efficieny is 0.26%</li>
</ul>
</div>

===

#### Systematics
![Systematics](img/systematics.png)<!-- .element height="50%" width="50%" style="float: right"-->
<div style="width: 50%; font-size: 60%">
<h6>Largest systematic: Hanle effect</h6>
<ul>
<li>if either the detection or repump laser are not linearly polarized, then there will be an imbalance in the number of left and right circularly polarized photons emitted</li>
<li>combined with any birefringence in the imaging system, this will result in a different measured efficiency for each circular polarization</li>
<li>this is suppressed by switching the direction of the quantization axis magnetic field, which reverses the Hanle effect</li>
</ul>
</div>

---

### Transition frequency
![Transition](img/transition.png)<!-- .element height="40%" width="40%" style="float: right"-->
<div style="width: 50%; font-size: 70%">
<ul>
<li>cycle photons on S-P transition for 2 $\mu$s, and then pump back to ground state</li>
<li>Scan an AOM frequency</li>
<li>fit to Lorentzian</li>
<li>Compare to $^{130}\text{Te}_2$ absorption lines in 10 cm long vapor cell. The $\text{Te}_2$ lines have an uncertainty of 50 MHz. Their wavemeter has an uncertainty of 2 MHz</li>
<li>Determine the $^{226}\text{Ra}+$ $^{2}\text{S}_{1/2} \to ^{2}\text{P}_{1/2}$ transitio frequency to 640.09663(6) THz</li>
</ul>
</div>

---


### What else is this good for?

- Parity/CP Violation
- Molecular Ions

===

<!-- .slide: data-background="img/its_a_trap.gif" -->

===

#### CP violation

- $^{225}\text{Ra}$ has been used to put a bound on the $CP$ violating Schiff moment (Argonne).
  - This used 15 mCi of neutral $^{225}\text{Ra}$, about 0.15 $\mu$g.

![Octupole Nucleus](img/octupole.png)<!-- .element height="50%" width="50%" "-->

===

#### Parity Violation

- low energy measurements of excess parity violation is probably the best way to search for dark matter
![Weinberg Angle](img/weinberg_angle.png)<!-- .element height="60%" width="60%" style="float: left"-->
![PV](img/PV.png)<!-- .element height="30%" width="30%" style="float: right"-->

===

#### Molecular Ions

- everyone knows that molecular ions are very interesting
  - rich internal structure
  - closely spaced states of opposite parity
  - high sensitivity to potential exotic physics
  - long range interactions
- Could use laser cooled trapped Ra$^+$ to make RaOH$^+$ or RaCOH$_3^+$
- in fact, that have already loaded RaOH$^+$ 



